import hashlib



#1. Implementa una funció hash del tipus SHA-1 per a encriptar la paraula hola. Com és la longitud de la
#hash de la paraula "Hola"?

    
#Funció per calcular el hash sha-1 de una cadena de text    
def sha1(missatge):    
    encriptacio_sha1 = hashlib.sha1()
    encriptacio_sha1.update(missatge.encode('utf-8'))
    return encriptacio_sha1.hexdigest()

        

#Exemple d'ús
paraula = "Hola"
encriptacio_sha1 = sha1(paraula)
print(f"La hash SHA-1 de la paraula {paraula} es: " , encriptacio_sha1)
print(f"La longitud és de: ",len(encriptacio_sha1))



#2.Quina és la longitud de la hash de les paraules «ola k ase»? (Mostra
#evidències)

# Cridem la funció del ex1
paraula = "ola k ase"
encriptacio_sha1 = sha1(paraula)
print(f"La hash SHA-1 de la paraula {paraula} es: " , encriptacio_sha1)
print(f"La longitud és de: ",len(encriptacio_sha1))

#3.Modifica el mètode SHA-1 per SHA-256 i SHA-512. Aprecies alguna
#diferència? (Mostra evidències)

# Funció SHA256
def sha256(missatge):    
    encriptacio_sha256 = hashlib.sha256()
    encriptacio_sha256.update(missatge.encode('utf-8'))
    return encriptacio_sha256.hexdigest()

# Funció SHA512
def sha512(missatge):
    encriptacio_sha512 = hashlib.sha512()
    encriptacio_sha512.update(missatge.encode('utf-8'))
    return encriptacio_sha512.hexdigest()


# Exemple d'ús
paraula2 = "Exemple SHA256"
encriptacio_sha256 = sha256(paraula2)
print(f"La hash SHA-256 de la paraula {paraula2} es: " ,encriptacio_sha256)
print(f"La longitud és de: ",len(encriptacio_sha256))

paraula3 = "Exemple SHA512"
encriptacio_sha512 = sha512(paraula3)
print(f"La hash SHA-512 de la paraula {paraula3} es: " ,encriptacio_sha512)
print(f"La longitud és de: ",len(encriptacio_sha512))

# La diferència entre les 2 és la longitud amb la que s'emmagatzema el xifrat, sha256 ocupa 64 bits i sha512 ocupa 128 bits.



#4. Utilitzant l’algoritme simètric AES Quina és la longitud de la clau simètrica de la paraula «Hola»?
#(Mostra evidències)

from cryptography.fernet import Fernet

def aes(missatge, key):
    cipher_suite = Fernet(key)
    missatge = missatge.encode('utf-8')
    encriptar = cipher_suite.encrypt(missatge)
    return encriptar

key = Fernet.generate_key()

missatge = "Hola jaja"

encriptar = aes(missatge,key)

print("Missatge encriptat:", encriptar)
print("La longitud és de;",len(encriptar))

# La evidència més notoria es la longitud dels bits a l'hora d'encriptar-se el missatge


#5. Quina és la longitud de la paraula «Hola» xifrada amb clau simètrica (DES)?
#( Mostra evidències)

import pyDes

def des_encrypt(key, plaintext):
    # Initialize the DES cipher with the provided key
    des = pyDes.des(key, pyDes.ECB, pad=None, padmode=pyDes.PAD_PKCS5)

    # Ensure that the plaintext is a multiple of 8 bytes by padding it
    padding = b'\0' * (8 - len(plaintext) % 8)
    plaintext += padding

    # Encrypt the plaintext
    ciphertext = des.encrypt(plaintext)

    return ciphertext

def des_decrypt(key, ciphertext):
    # Initialize the DES cipher with the provided key
    des = pyDes.des(key, pyDes.ECB, pad=None, padmode=pyDes.PAD_PKCS5)

    # Decrypt the ciphertext
    plaintext = des.decrypt(ciphertext)

    return plaintext

# Example usage
if __name__ == "__main__":
    key = b'8bytekey'  # 8-byte key for DES
    plaintext = b'This is a test'

    # Encrypt
    ciphertext = des_encrypt(key, plaintext)
    print("Ciphertext:", ciphertext)

    # Decrypt
    decrypted_text = des_decrypt(key, ciphertext)
    print("Decrypted Text:", decrypted_text.decode('utf-8'))
    


#6. Mostra una captura de la paraula «Hola» xifrada amb clau simètrica (DES)


#7. Quina és la longitud de les paraules «ola k ase» xifrada amb clau simètrica(DES)? ( Mostra
#evidències)
#8. Quina és la longitud de les paraules «ola k ase» xifrada amb clau simètrica (AES/ECB)? ( Mostra
#evidències)
#9. Mostra una captura de la paraula «Hola» xifrada amb clau asimètrica (RSA)
#10. Quina és la longitud de les paraules «ola k ase» xifrada amb clau asimètrica (RSA)? ( Mostra
#evidències)
#11. Quina és la longitud de la clau privada de l’algoritme RSA? ( Mostra evidències) Quina és la
#longitud de la clau pública de l’algoritme RSA? ( Mostra evidències) Implementa un exemple de com
#generar un parell de claus RSA
#12. Programa una funció hash de collita pròpia . Ha d’estar en un objecte a part i no pot contenir codi
#en espagueti.