

list = ["A","B","C","D","E","F","G","H","I","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]


opcio = str(input("Que vols fer: \n a)Xifrar \n b)Desxifrar \n Opcio: "))


if opcio == "a":
    missatge = str(input("Introdueix el text a xifrar: "))
    desplaçament = int(input("Introdueix el número de posicions a desplaçar: "))
    xifrat = ""
    for i in missatge:
        if i in list:
            xifrat += list[(list.index(i) + desplaçament % (len(list)))]
    print(xifrat)

elif opcio == "b":
    missatge = str(input("Introdueix el text a xifrar: "))
    desplaçament = int(input("Introdueix el número de posicions a desplaçar: "))
    desxifrat = ""
    for i in missatge:
        if i in list:
            desxifrat += list[(list.index(i) - desplaçament % (len(list)))]
    print(desxifrat)



