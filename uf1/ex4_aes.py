#!/usr/bin/python3

#4. Utilitzant l’algoritme simètric AES Quina és la longitud de la clau simètrica de la paraula «Hola»?
#(Mostra evidències)

#!/usr/bin/python3

from cryptography.fernet import Fernet

def aes(missatge, key):
    cipher_suite = Fernet(key)
    missatge = missatge.encode('utf-8')
    encriptar = cipher_suite.encrypt(missatge)
    return encriptar

key = Fernet.generate_key()

missatge = "Marti"

encriptar = aes(missatge,key)

print("Missatge encriptat:", encriptar)
print("La longitud és de;",len(encriptar))